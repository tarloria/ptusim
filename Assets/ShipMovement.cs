﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
    }

    System.Random Rand = new System.Random();

    float XTemp = 0;

    public float Amplitude = 0.03f;
    

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }

        float rotation1 = Amplitude * (float)(Math.Cos((Time.time * 1.0) + 10) * 180.0 / 3.14159);
        float rotation2 = Amplitude * (float)(Math.Cos((Time.time * 1.0) + 20) * 180.0 / 3.14159);
        float rotation3 = Amplitude * (float)(Math.Cos((Time.time * 1.0) + 30) * 180.0 / 3.14159);

        Debug.Log("Amplitude: " + Amplitude.ToString() + " " +
            rotation1.ToString() + " " +
            rotation2.ToString() + " " +
            rotation3.ToString());

        transform.rotation = Quaternion.Euler(rotation1, rotation2, rotation3);

        //transform.Rotate(new Vector3(-90, 0, 0));

        //Debug.Log(rotation1.ToString() + " " + rotation2.ToString() + " " + rotation3.ToString());
    }

    public void SetAmplitude(float AmplitudeIn)
    {
        Amplitude = AmplitudeIn;
    }
}

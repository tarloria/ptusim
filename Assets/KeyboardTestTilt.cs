﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardTestTilt : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    private float rotationSpeed = 50.0f;

    // Update is called once per frame
    void Update()
    {
        // Get the horizontal and vertical axis.
        // By default they are mapped to the arrow keys.
        // The value is in the range -1 to 1
        float RotationTilt = Input.GetAxis("Vertical") * rotationSpeed;

        // Make it move 10 meters per second instead of 10 meters per frame...        
        RotationTilt *= Time.deltaTime;        

        // Rotate around our y-axis
        transform.Rotate(-RotationTilt, 0, 0);
    }

    public void SetSpeed(float SpeedIn)
    {
        rotationSpeed = SpeedIn;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardTestPan : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    private float rotationSpeed = 50.0f;
    
    // Update is called once per frame
    void Update()
    {
        // Get the horizontal and vertical axis.
        // By default they are mapped to the arrow keys.
        // The value is in the range -1 to 1
        float RotationPan = Input.GetAxis("Horizontal") * rotationSpeed;

        RotationPan *= Time.deltaTime;

        // Rotate around our y-axis
        transform.Rotate(0, RotationPan, 0);
    }

    public void SetSpeed(float SpeedIn)
    {
        rotationSpeed = SpeedIn;
    }
}
